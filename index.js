(function () {
    var GiphyApp = angular.module("GiphyApp", []);

    var GiphyCtrl = function ($http) {
        var giphyCtrl = this;

        giphyCtrl.subject = "";
        giphyCtrl.image = "/landing-img.jpg";

        giphyCtrl.search = function () {
            if (!giphyCtrl.subject || (giphyCtrl.subject.trim().length <= 0))
                return
            console.log("searching %s", giphyCtrl.subject);
            
            var promise = $http.get("https://api.giphy.com/v1/gifs/random", {
                params: {
                    api_key: "dc6zaTOxFJmzC",
                    tag: giphyCtrl.subject
                }
            });
            promise.then(function(result) {
                giphyCtrl.image = result.data.data.image_url;
            });
            promise.catch(function(status) {
                for (var i in status)
                    console.info("%s Status: %s", i, status[i]); // print in black
                    // console.error("%s Status: %s", i, status[i]); // print in red
            });
        };
    };
    GiphyCtrl.$inject = ["$http"];

    GiphyApp.controller("GiphyCtrl", GiphyCtrl);
})();